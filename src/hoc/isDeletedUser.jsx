import { useContext } from 'react';
import AppContext from '../providers/AppContext';
import { Navigate } from 'react-router';
import { userRole } from '../common/user-role';
import { logOutUser } from '../services/auth.services';

import * as React from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Slide from '@mui/material/Slide';
import { useState } from 'react';
import { useNavigate } from 'react-router-dom';

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default function IsDeletedUser({ children }) {
  const { user, userData, setContext } = useContext(AppContext);
  const navigate = useNavigate();
  const [open, setOpen] = useState(false);

  const handleContacts = () => {
    handleClose();
    navigate('/about');
  }

  const handleClose = () => {
    setOpen(false);
  };
  
  if (userData?.role === userRole.DELETED) {
    logOutUser()
      .then(() => {
        setContext({
          user: null,
          userData: null,
        });
        setOpen(true);

      })
      .catch(console.error);
  }

  return (
    <>
    <div>
      <Dialog
        open={open}
        TransitionComponent={Transition}
        keepMounted
        onClose={handleClose}
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle>{"Account has been deleted"}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-slide-description">
            It seems that you are trying to use deleted account.
            If you want to use this account again, please contact our admins to activate it.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleContacts}>Contacts</Button>
        </DialogActions>
      </Dialog>
    </div>
    {children}
    </>
  );
}
