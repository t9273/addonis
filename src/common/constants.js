export const constants = {
  USER_NAME_MIN_LENGTH: 2,
  USER_NAME_MAX_LENGTH: 20,
  USER_PHONE_NUM_LENGTH: 10,
  ADDON_NAME_MIN_LENGTH: 3,
  ADDON_NAME_MAX_LENGTH: 30,
  ADDONS_PER_PAGE: 12,
  MONTHLY_DONATION_TARGET: 500,
  DEFAULT_LOGO_URL:
    'https://firebasestorage.googleapis.com/v0/b/addonis-project.appspot.com/o/addOns%2FdefaultLogo%2FdefaultLogo.png?alt=media&token=ac684a76-bcc9-46ca-96fa-f70aff501c35',
  SPINNER_URL:
    'https://firebasestorage.googleapis.com/v0/b/addonis-project.appspot.com/o/loadingSpinner%2FSpinner-1s-400px.gif?alt=media&token=e8b5f66c-f8fd-428d-8307-20e94c3da31c',
  BTD_AVATAR: 
    'https://firebasestorage.googleapis.com/v0/b/addonis-project.appspot.com/o/addOns%2Fbtd_avatar%2Fbtd_logo_white.png?alt=media&token=98ac1aa4-4010-4c58-a28b-73b741dd372c',
  BTD_LOGO: 'https://firebasestorage.googleapis.com/v0/b/addonis-project.appspot.com/o/addOns%2Fbtd_avatar%2Fbtd_avatar.png?alt=media&token=2fabb818-9d48-4330-819a-cfa2d88a7d13',
  PAGE_NOT_FOUND:
  'https://firebasestorage.googleapis.com/v0/b/addonis-project.appspot.com/o/images%2Fpage_not_found%2Ferror_page.png?alt=media&token=676950ad-7a22-4eea-8fcf-2b800f552206',
};
