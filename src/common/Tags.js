export const Tags = {
  "Administration Tools":true,
"Android":true,
"Blockchain":true,
"Build":true,
"Cloud":true,
"Code Editing":true,
"Code Quality":true,
"Code Tools":true,
"Completion":true,
"Data Science":true,
"Database":true,
"Debugging":true,
"Editor":true,
"Editor Color Schemes":true,
"Education":true,
"Folder":true,
"Formatting":true,
"Framework":true,
"Fun Stuff":true,
"GUI Builder":true,
"Graphics":true,
"Inspection":true,
"Internet of Things":true,
"Issue Trackers":true,
"J2ME":true,
"JBehave":true,
"JEE":true,
"JavaScript":true,
"Localization":true,
"MPS":true,
"Machine Learning":true,
"Maven":true,
"Menu Components":true,
"Miscellaneous":true,
"Modeling and CASE tools":true,
"Navigation":true,
"Network":true,
"Notification and Visualizers":true,
"OS Integration":true,
"Obfuscation":true,
"Plugin Development":true,
"Profiling":true,
"Programming Language":true,
"Refactoring":true,
"Remote APIs":true,
"Reporting":true,
"Robotics":true,
"Search and Replace":true,
"Security":true,
"Snippets":true,
"Spellcheck":true,
"Static Analysis":true,
"TeamWork":true,
"Testing":true,
"Time Management":true,
"Tools Integration":true,
"User Interface":true,
"VCS":true,
"Viewer":true,
"Web":true,
"XML":true,
"iOS":true,
}