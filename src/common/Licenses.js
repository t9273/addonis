export const Licenses = {
"Apache license 2.0" : true,
"MIT license" : true,
"GNU General Public License (GPL)" : true,
"BSD 3-Clause “New” or “Revised” license" : true,
"BSD 2-Clause “Simplified” or “FreeBSD” license" : true,
"Mozilla Public License 2.0" : true,
"Eclipse Public License" : true,
"GNU Lesser General Public License" : true,
}