import Box from '@mui/material/Box';
import './Home.css';
import Carousel from 'react-multi-carousel';
import AddonCard from '../../components/AddonCard/AddonCard';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import { carouselConfig } from '../../configs/carousel.config';
import { useQuery } from 'react-query';
import { getAllApproveAddons } from '../../services/addons.services';
import {
  getAllUsersData,
  usersFromSnapshot,
} from '../../services/users.services';
import { constants } from '../../common/constants';

export default function Home() {
  const { data: addonsData } = useQuery(['addonsData'], () => {
    return getAllApproveAddons()
      .then((addons) => addons)
      .catch(console.error);
  });

  const { data: featured } = useQuery(
    ['featured', addonsData],
    () => {
      return [...addonsData].filter((addon) => addon.featured);
    },
    { enabled: !!addonsData }
  );

  const { data: trending } = useQuery(
    ['trending', addonsData],
    () => {
      return [...addonsData].sort((a, b) => b.downloadCount - a.downloadCount);
    },
    { enabled: !!addonsData }
  );

  const { data: latest } = useQuery(
    ['latest', addonsData],
    () => {
      return [...addonsData].sort((a, b) => b.uploadedOn - a.uploadedOn);
    },
    { enabled: !!addonsData }
  );

  const { data: downloads } = useQuery(
    ['downloads', addonsData],
    () => {
      return [...addonsData].reduce((acc, addon) => {
        if (addon.downloads) {
          acc += Object.keys(addon.downloads).length;
        }
        return acc;
      }, 0);
    },
    { enabled: !!addonsData }
  );

  const { data: usersData } = useQuery(['usersData'], () => {
    return getAllUsersData()
      .then((snapshot) => {
        return usersFromSnapshot(snapshot);
      })
      .catch(console.error);
  });

  return (
    <Box maxWidth={1000} mt={3} ml={'auto'} mr={'auto'} position={'relative'}>
      {!featured || !trending || !latest ? (
        <Box minHeight={'75vh'} textAlign='center'>
          <img src={constants.SPINNER_URL} alt='loading spinner' />
        </Box>
      ) : (
        <>
          <Typography variant='h4' ml={3}>
            Featured
          </Typography>
          {featured && (
            <Carousel {...carouselConfig}>
              {featured.slice(0, 12).map((addon, index) => {
                return (
                  <AddonCard
                    key={index}
                    title={addon.name}
                    author={addon.author}
                    rating={+addon.averageRating}
                    imgSrc={
                      addon.image ? addon.image : constants.DEFAULT_LOGO_URL
                    }
                    price={addon.price ? addon.price : 'FREE'}
                    downloads={addon.downloadCount}
                  />
                );
              })}
            </Carousel>
          )}
          <Typography variant='h4' ml={3}>
            Trending
          </Typography>
          {trending && (
            <Carousel {...carouselConfig}>
              {trending.slice(0, 24).map((addon, index) => {
                return (
                  <AddonCard
                    key={index}
                    title={addon.name}
                    author={addon.author}
                    rating={+addon.averageRating}
                    imgSrc={
                      addon.image ? addon.image : constants.DEFAULT_LOGO_URL
                    }
                    price={addon.price ? addon.price : 'FREE'}
                    downloads={addon.downloadCount}
                  />
                );
              })}
            </Carousel>
          )}
          <Typography variant='h4' ml={3}>
            New
          </Typography>
          {latest && (
            <Carousel {...carouselConfig}>
              {latest.slice(0, 24).map((addon, index) => {
                return (
                  <AddonCard
                    key={index}
                    title={addon.name}
                    author={addon.author}
                    rating={+addon.averageRating}
                    imgSrc={
                      addon.image ? addon.image : constants.DEFAULT_LOGO_URL
                    }
                    price={addon.price ? addon.price : 'FREE'}
                    downloads={addon.downloadCount}
                  />
                );
              })}
            </Carousel>
          )}
          {addonsData && usersData && (
            <Box maxWidth={1000} className='StatsBox'>
              <Grid
                container
                color={'darkslategray'}
                m={3}
                textAlign={'center'}
                justifyContent='space-around'
              >
                <Grid item>
                  <Typography variant='h4'>{usersData.length}</Typography>
                  <Typography variant='h6'>Registered users</Typography>
                </Grid>
                <Grid item>
                  <Typography variant='h4'>{addonsData.length}</Typography>
                  <Typography variant='h6'>Addons uploaded</Typography>
                </Grid>
                <Grid item>
                  <Typography variant='h4'>{downloads}</Typography>
                  <Typography variant='h6'>Total downloads</Typography>
                </Grid>
              </Grid>
            </Box>
          )}
        </>
      )}
    </Box>
  );
}
