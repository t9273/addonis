import * as React from 'react';
import Box from '@mui/material/Box';
import Tabs from '@mui/material/Tabs';
import TabPanel from '../../components/TabPanel/TabPanel';
import Tab from '@mui/material/Tab';
import { useState } from 'react';
import Addons from '../Addons/Addons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBell, faUsers } from '@fortawesome/free-solid-svg-icons';
import AllUsers from '../AllUsers/AllUsers';
import { addOnState } from '../../common/addOnState';

const tabConfig = () => {
  return {
    display: 'inline-flex',
    justifyContent: 'flex-start',
    flexDirection: 'row',
  };
};

export default function AdminDashboard() {
  const [tabValue, setTabValue] = useState(0);

  const handleChange = (event, newValue) => {
    setTabValue(newValue);
  };

  return (
    <Box sx={{ width: '100%', bgcolor: 'background.paper' }}>
      <Tabs
        value={tabValue}
        onChange={handleChange}
        centered
        sx={{ paddingBottom: 4 }}
      >
        <Tab
          sx={tabConfig}
          label={
            <>
              <FontAwesomeIcon className='TabIcon' icon={faBell} />
              Pending
            </>
          }
        />
        <Tab
          sx={tabConfig}
          label={
            <>
              <FontAwesomeIcon className='TabIcon' icon={faUsers} />
              Users
            </>
          }
        />
      </Tabs>
      <TabPanel value={tabValue} index={0}>
        <Addons pending={addOnState.PENDING} />
      </TabPanel>
      <TabPanel value={tabValue} index={1}>
        <AllUsers />
      </TabPanel>
    </Box>
  );
}
