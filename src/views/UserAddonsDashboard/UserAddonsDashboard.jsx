import { useContext, useState } from 'react';
import Modal from '@mui/material/Modal';
import AddonChart from '../../components/Charts/AddonChart';
import Stack from '@mui/material/Stack';

import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import PropTypes from 'prop-types';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import TableSortLabel from '@mui/material/TableSortLabel';
import Paper from '@mui/material/Paper';
import { visuallyHidden } from '@mui/utils';
import Rating from '@mui/material/Rating';
import Tooltip from '@mui/material/Tooltip';
import IconButton from '@mui/material/IconButton';
import VisibilityIcon from '@mui/icons-material/Visibility';
import AnalyticsIcon from '@mui/icons-material/Analytics';
import { useNavigate } from 'react-router-dom';
import AppContext from '../../providers/AppContext';
import { useQuery } from 'react-query';
import { getAddonsByAuthor } from '../../services/addons.services';
import moment from 'moment';
import { Grid } from '@mui/material';

const descendingComparator = (a, b, orderBy) => {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
};

const getComparator = (order, orderBy) => {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
};

const headCells = [
  {
    id: 'name',
    numeric: false,
    disablePadding: true,
    label: 'Add-on Name',
  },
  {
    id: 'averageRating',
    numeric: true,
    disablePadding: false,
    label: 'Average Rating',
  },
  {
    id: 'downloadCount',
    numeric: true,
    disablePadding: false,
    label: 'Downloads',
  },
  {
    id: 'uploadedOn',
    numeric: true,
    disablePadding: false,
    label: 'Uploaded On',
  },
  {
    id: 'state',
    numeric: true,
    disablePadding: false,
    label: 'Add-on Status',
  },
];

const modalStatStyle = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  maxWidth: 800,
  minWidth: 600,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};

const EnhancedTableHead = ({ order, orderBy, onRequestSort }) => {
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={headCell.numeric ? 'right' : 'left'}
            padding='normal'
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <Box component='span' sx={visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </Box>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
};

EnhancedTableHead.propTypes = {
  onRequestSort: PropTypes.func.isRequired,
  order: PropTypes.oneOf(['asc', 'desc']).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};

const UserAddonsDashboard = () => {
  const navigate = useNavigate();
  const {
    userData: { handle },
  } = useContext(AppContext);

  const [order, setOrder] = useState('asc');
  const [orderBy, setOrderBy] = useState('date');
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);

  const initialStartDate = new Date(
    new Date().getFullYear(),
    new Date().getMonth() - 11,
    1
  );
  const initialEndDate = new Date();
  const [selectedAddon, setSelectedAddon] = useState();
  const [showModal, setShowModal] = useState(false);
  const [startDate, setStartDate] = useState(initialStartDate);
  const [endDate, setEndDate] = useState(initialEndDate);

  const handleOpen = (addon) => {
    setSelectedAddon(addon);
    setShowModal(true);
  };

  const handleClose = () => {
    setShowModal(false);
    setStartDate(initialStartDate);
    setEndDate(initialEndDate);
  };

  const handleStartDate = (e) => {
    const newDate = new Date(e.target.value);
    if (newDate.getTime() > endDate.getTime()) {
      alert('Begin date should be before End one');
      return;
    } else {
      setStartDate(newDate);
    }
  };

  const handleEndDate = (e) => {
    const newDate = new Date(e.target.value);
    if (newDate.getTime() < startDate.getTime()) {
      alert('End date should be after Start one');
      return;
    } else {
      setEndDate(newDate);
    }
  };

  const dateStringFormatter = (date) => {
    return date.toISOString().split('T')[0];
  };

  const { data: userAddons } = useQuery(
    ['userAddons', handle],
    () => {
      return getAddonsByAuthor(handle);
    },
    {
      enabled: !!handle,
    }
  );

  const handleRequestSort = (_, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleTableSort = () => {
    return getComparator(order, orderBy);
  };

  const handleChangePage = (_, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const calculateAverageRatings = () => {
    if(userAddons.length === 0) return 0;
    return (
      
      userAddons.reduce((acc, addon) => {
        if (addon.averageRating !== 0) {
          acc += +addon.averageRating;
        }
        return acc;
      }, 0) / userAddons.filter((addon) => +addon.averageRating !== 0).length
    );
  };

  // Avoid a layout jump when reaching the last page with empty rows.
  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - userAddons.length) : 0;

  return (
    userAddons && (
      <Box sx={{ width: '100%' }}>
        <Box
          mb={3}
          display='grid'
          gridTemplateColumns={'1fr 1fr 1fr'}
          columnGap={2}
        >
          <Paper elevation={12} sx={{ textAlign: 'center', p: 3 }}>
            <Typography variant='h6'>Uploaded Add-ons</Typography>
            <Typography variant='h5'>{userAddons.length}</Typography>
          </Paper>
          <Paper elevation={12} sx={{ textAlign: 'center', p: 3 }}>
            <Typography variant='h6'>Average Rating</Typography>
            <Typography
              variant='h5'
              component={'div'}
              sx={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <Rating
                value={calculateAverageRatings()}
                precision={0.1}
                readOnly
              />
              {calculateAverageRatings() === 0 ? 0 : calculateAverageRatings().toFixed(2)}
            </Typography>
          </Paper>
          <Paper elevation={12} sx={{ textAlign: 'center', p: 3 }}>
            <Typography variant='h6'>Total Downloads</Typography>
            <Typography variant='h5'>
              {userAddons.reduce((acc, addon) => acc + addon.downloadCount, 0)}
            </Typography>
          </Paper>
        </Box>
        <Paper elevation={12} sx={{ width: '100%', mb: 2 }}>
          <Typography
            sx={{ ml: 2 }}
            variant='h6'
            id='tableTitle'
            component='div'
          >
            Your Add-ons
          </Typography>
          <TableContainer>
            <Table
              sx={{ minWidth: 750 }}
              aria-labelledby='tableTitle'
              size='medium'
            >
              <EnhancedTableHead
                order={order}
                orderBy={orderBy}
                onRequestSort={handleRequestSort}
                rowCount={userAddons.length}
              />
              <TableBody>
                {userAddons
                  .slice()
                  .sort(handleTableSort())
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((addon, index) => {
                    const labelId = `enhanced-table-checkbox-${index}`;

                    return (
                      <TableRow
                        hover
                        role='checkbox'
                        tabIndex={-1}
                        key={addon.name}
                      >
                        <TableCell
                          component='th'
                          id={labelId}
                          scope='row'
                          padding='normal'
                          sx={{ minWidth: 250 }}
                        >
                          {addon.name}
                        </TableCell>
                        <TableCell
                          align='right'
                          component='th'
                          id={labelId}
                          scope='row'
                          padding='normal'
                        >
                          {
                            <Tooltip
                              title={`Average rating: ${addon.averageRating}`}
                            >
                              <Typography variant='body2' component={'span'}>
                                <Rating readOnly value={+addon.averageRating} />
                              </Typography>
                            </Tooltip>
                          }
                        </TableCell>
                        <TableCell align='right'>
                          {addon.downloadCount}
                        </TableCell>
                        <TableCell align='right'>
                          {moment(addon.uploadedOn).format('ll')}
                        </TableCell>
                        <TableCell align='right'>
                          {addon.state === 1
                            ? 'Pending'
                            : addon.state === 2
                            ? 'Approved'
                            : 'Rejected'}
                        </TableCell>
                        <TableCell align='right'>
                          <Tooltip title='See addon statistics'>
                            <IconButton onClick={() => handleOpen(addon)}>
                              <AnalyticsIcon />
                            </IconButton>
                          </Tooltip>
                        </TableCell>
                        <TableCell align='right'>
                          <Tooltip title='View addon'>
                            <IconButton
                              onClick={() => navigate(`/addons/${addon.name}`)}
                            >
                              <VisibilityIcon />
                            </IconButton>
                          </Tooltip>
                        </TableCell>
                      </TableRow>
                    );
                  })}
                {selectedAddon && (
                  <Modal
                    open={showModal}
                    onClose={handleClose}
                    aria-labelledby='modal-modal-title'
                    aria-describedby='modal-modal-description'
                  >
                    <Box sx={modalStatStyle}>
                      <Stack direction='row' spacing={8} sx={{ gap: 4, p: 3 }}>
                        <TextField
                          id='end_date'
                          label='Start Date'
                          type='date'
                          defaultValue={dateStringFormatter(initialStartDate)}
                          sx={{ width: 220 }}
                          onChange={(e) => handleStartDate(e)}
                          InputLabelProps={{
                            shrink: true,
                          }}
                        />
                        <TextField
                          id='end_date'
                          label='End Date'
                          type='date'
                          defaultValue={dateStringFormatter(initialEndDate)}
                          onChange={(e) => handleEndDate(e)}
                          sx={{ width: 220 }}
                          InputLabelProps={{
                            shrink: true,
                          }}
                        />
                      </Stack>
                      <Grid sx={{ p: 1 }}>
                        <Grid item>
                          <AddonChart
                            addonData={selectedAddon}
                            lowerLimit={startDate}
                            upperLimit={endDate}
                          />
                        </Grid>
                      </Grid>
                    </Box>
                  </Modal>
                )}
                {emptyRows > 0 && (
                  <TableRow
                    style={{
                      height: 73 * emptyRows,
                    }}
                  >
                    <TableCell colSpan={6} />
                  </TableRow>
                )}
              </TableBody>
            </Table>
          </TableContainer>
          <TablePagination
            rowsPerPageOptions={[5, 10, 25]}
            component='div'
            count={userAddons.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
        </Paper>
      </Box>
    )
  );
};

export default UserAddonsDashboard;
