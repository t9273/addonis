import { useContext, useEffect, useState } from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import AppContext from '../../providers/AppContext';
import {
  createUserHandle,
  getLiveUsers,
  getUserByHandle,
  usersFromSnapshot,
} from '../../services/users.services';
import { registerUser } from '../../services/auth.services';
import { useNavigate } from 'react-router-dom';
import { constants } from '../../common/constants';
import { sendEmailVerification } from 'firebase/auth';

const theme = createTheme();

export default function SignUp() {
  const { setContext } = useContext(AppContext);
  const navigate = useNavigate();
  let [errMessage, setErrMessage] = useState('');
  const [users, setUsers] = useState([]);
  const actionCodeSettings = {
    url: 'http://localhost:3000/signIn',
    handleCodeInApp: true,
  };

  useEffect(() => {
    getLiveUsers((snapshot) => {
      if (!snapshot.exists()) return null;

      setUsers(usersFromSnapshot(snapshot));
    });
  }, [users.length]);

  const handleRegister = (event) => {
    event.preventDefault();

    const data = new FormData(event.currentTarget);
    const formObject = {
      email: data.get('email'),
      password: data.get('password'),
      userName: data.get('userName'),
      mobile: data.get('mobile'),
    };

    if (
      formObject.userName.length < constants.USER_NAME_MIN_LENGTH ||
      formObject.userName.length > constants.USER_NAME_MAX_LENGTH
    ) {
      return setErrMessage(
        `user name should be between ${constants.USER_NAME_MIN_LENGTH} and ${constants.USER_NAME_MAX_LENGTH} symbols`
      );
    }

    const mobileRegex = /^[0-9]{10}$/;

    if (
      formObject.mobile.length !== constants.USER_PHONE_NUM_LENGTH ||
      !mobileRegex.test(+formObject.mobile)
    ) {
      return setErrMessage(`Invalid mobile number`);
    }

    if (users.some((user) => user.mobile === formObject.mobile)) {
      return setErrMessage(`Phone number has already been registered`);
    }

    if (!formObject.email.includes('@')) {
      return setErrMessage('Invalid E-mail !');
    }

    if (formObject.password.length < 6) {
      return setErrMessage('Password should be at least 6 characters');
    }

    getUserByHandle(formObject.userName)
      .then((snapshot) => {
        if (snapshot.exists()) {
          return setErrMessage(
            `user name: ${formObject.userName} already exists!`
          );
        }

        return registerUser(formObject.email, formObject.password)
          .then((result) => {
            sendEmailVerification(result.user, actionCodeSettings).catch(
              console.error
            );

            return createUserHandle(
              formObject.userName,
              result.user.uid,
              formObject.mobile,
              result.user.email,
              result.user.emailVerified
            ).then(() => {
              getUserByHandle(formObject.userName)
                .then((r) => {
                  setContext({
                    user: result.user,
                    userData: r.val(),
                  });
                })
                .catch(console.error);
              navigate('../home');
            });
          })
          .catch((error) => {
            if (error.message.includes(`email-already-in-use`)) {
              return setErrMessage(
                `Email ${formObject.email} has already been registered!`
              );
            }
          });
      })
      .catch((err) => console.log('main chain ' + err.message));
  };

  return (
    <ThemeProvider theme={theme}>
      <Container component='main' maxWidth='xs'>
        <CssBaseline />
        <Box
          minHeight={'69vh'}
          sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component='h1' variant='h5'>
            Sign up
          </Typography>
          <Box
            component='form'
            noValidate
            onSubmit={handleRegister}
            sx={{ mt: 3 }}
          >
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <TextField
                  autoComplete='off'
                  name='userName'
                  required
                  fullWidth
                  id='userName'
                  label='userName'
                  autoFocus
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  fullWidth
                  id='mobile'
                  label='mobile'
                  name='mobile'
                  autoComplete='off'
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  id='email'
                  label='Email Address'
                  name='email'
                  autoComplete='off'
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  name='password'
                  label='Password'
                  type='password'
                  id='password'
                  autoComplete='off'
                />
              </Grid>
            </Grid>
            {errMessage && <span style={{ color: 'red' }}>{errMessage}</span>}
            <Button
              type='submit'
              fullWidth
              variant='contained'
              sx={{ mt: 3, mb: 2 }}
            >
              Sign Up
            </Button>
            <Grid container justifyContent='flex-end'>
              <Grid item>
                <Link href='/signIn' variant='body2'>
                  Already have an account? Sign in
                </Link>
              </Grid>
            </Grid>
          </Box>
        </Box>
      </Container>
    </ThemeProvider>
  );
}
