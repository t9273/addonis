import { Fragment, useEffect, useState } from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Tooltip from '@mui/material/Tooltip';
import IconButton from '@mui/material/IconButton';
import DeleteIcon from '@mui/icons-material/Delete';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import BlockIcon from '@mui/icons-material/Block';
import Avatar from '@mui/material/Avatar';

import moment from 'moment';
import Button from '@mui/material/Button';
import {
  getLiveUsers,
  updateUserRole,
  usersFromSnapshot,
} from '../../services/users.services';
import { userRole } from '../../common/user-role';
import { deleteUser } from '../../services/users.services';
import SearchIcon from '@mui/icons-material/Search';
import { styled, alpha } from '@mui/material/styles';
import InputBase from '@mui/material/InputBase';

const Search = styled('div')(({ theme }) => ({
  position: 'relative',
  borderRadius: theme.shape.borderRadius,
  backgroundColor: alpha(theme.palette.common.white, 0.15),
  '&:hover': {
    backgroundColor: alpha(theme.palette.common.white, 0.25),
  },
  marginRight: theme.spacing(2),
  marginLeft: 0,
  width: '100%',
  [theme.breakpoints.up('sm')]: {
    marginLeft: theme.spacing(3),
    width: 'auto',
  },
}));

const SearchIconWrapper = styled('div')(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: '100%',
  position: 'absolute',
  pointerEvents: 'none',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: 'inherit',
  '& .MuiInputBase-input': {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: '20ch',
    },
  },
}));

const headCells2 = [
  {
    id: 'User',
    numeric: false,
    disablePadding: true,
    label: 'User',
  },
  {
    id: 'Registered',
    numeric: true,
    disablePadding: false,
    label: 'Registered',
  },
  {
    id: 'Uploaded addons',
    numeric: true,
    disablePadding: false,
    label: 'Uploaded addons',
  },
];

const EnhancedTableHead = () => {
  return (
    <TableHead>
      <TableRow>
        {headCells2.map((headCell) => (
          <TableCell key={headCell.id} align='center' padding='normal'>
            <h2>{headCell.label}</h2>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
};

const AllUsers = () => {
  const [open, setOpen] = useState(false);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [users, setUsers] = useState([]);
  const [filteredUsers, setFilteredUsers] = useState([]);

  useEffect(() => {
    getLiveUsers((snapshot) => {
      if (!snapshot.exists()) return null;

      setUsers(usersFromSnapshot(snapshot));
      setFilteredUsers(usersFromSnapshot(snapshot));
    });
  }, [users.length]);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleChangePage = (_, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleDelete = (handle, imgUrl) => {
    deleteUser(handle, imgUrl);
    setOpen(false);
  };

  const handleBlockUser = (handle, role) => {
    role !== userRole.BANNED
      ? updateUserRole(handle, userRole.BANNED)
      : updateUserRole(handle, userRole.ORDINARY);
  };

  // Avoid a layout jump when reaching the last page with empty rows.
  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - users?.length) : 0;

  const handleSearch = (event) => {
    setFilteredUsers(
      users.filter((user) =>
        user.handle.toLowerCase().includes(event.target.value.toLowerCase())
      )
    );
  };

  return (
    <Box sx={{ width: '100%' }}>
      <Paper elevation={12} sx={{ width: '100%', mb: 2 }}>
        <Search>
          <SearchIconWrapper>
            <SearchIcon />
          </SearchIconWrapper>
          <StyledInputBase
            onChange={handleSearch}
            placeholder='Search user …'
            inputProps={{ 'aria-label': 'search' }}
          />
        </Search>
        <TableContainer>
          <Table
            sx={{ minWidth: 750 }}
            aria-labelledby='tableTitle'
            size='medium'
          >
            <EnhancedTableHead
              rowCount={filteredUsers ? filteredUsers.length : 0}
            />
            <TableBody>
              {filteredUsers &&
                filteredUsers
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row, index) => {
                    const labelId = `enhanced-table-checkbox-${index}`;

                    return (
                      <TableRow
                        hover
                        role='checkbox'
                        tabIndex={-1}
                        key={row.uid}
                      >
                        <TableCell
                          component='th'
                          id={labelId}
                          scope='row'
                          padding='normal'
                          align='center'
                          sx={{ minWidth: 250 }}
                        >
                          <>
                            {row.avatarURL ? (
                              <Avatar
                                alt='Remy Sharp'
                                src={`${row.avatarURL}`}
                              />
                            ) : (
                              <Avatar
                                alt='Remy Sharp'
                                src={`https://robohash.org/${row.handle}?set=set3`}
                              />
                            )}
                            {row.handle}
                          </>
                        </TableCell>
                        <TableCell align='center'>
                          {
                            <Tooltip title={row.createdOn.toLocaleString()}>
                              <Typography variant='body2' component={'span'}>
                                {moment(row.createdOn).fromNow()}
                              </Typography>
                            </Tooltip>
                          }
                        </TableCell>
                        <TableCell align='center'>
                          {row.addOns ? Object.keys(row.addOns).length : 0}
                        </TableCell>
                        <TableCell align='center'></TableCell>
                        <TableCell align='center'>
                          {
                            <IconButton
                              onClick={() =>
                                handleBlockUser(row.handle, row.role)
                              }
                            >
                              <BlockIcon
                                color={
                                  row.role === userRole.BANNED
                                    ? 'error'
                                    : 'inherit'
                                }
                              />
                            </IconButton>
                          }
                        </TableCell>
                        <TableCell align='center'>
                          <IconButton onClick={handleClickOpen}>
                            <DeleteIcon
                              color={
                                row.role === userRole.DELETED
                                  ? 'error'
                                  : 'inherit'
                              }
                            />
                          </IconButton>
                          {open && (
                            <Fragment>
                              <Dialog
                                open={open}
                                onClose={handleClose}
                                aria-labelledby='alert-dialog-title'
                                aria-describedby='alert-dialog-description'
                              >
                                <DialogTitle id='alert-dialog-title'>
                                  {'Deleting user'}
                                </DialogTitle>
                                <DialogContent>
                                  <DialogContentText id='alert-dialog-description'>
                                    {`You are going to delete user: ${row.handle}`}
                                  </DialogContentText>
                                </DialogContent>
                                <DialogActions>
                                  <Button onClick={handleClose}>Cancel</Button>
                                  <Button
                                    onClick={() =>
                                      handleDelete(
                                        row.handle,
                                        row.avatarURL ? row.avatarURL : []
                                      )
                                    }
                                    autoFocus
                                  >
                                    Delete
                                  </Button>
                                </DialogActions>
                              </Dialog>
                            </Fragment>
                          )}
                        </TableCell>
                      </TableRow>
                    );
                  })}
              {emptyRows > 0 && (
                <TableRow
                  style={{
                    height: 73 * emptyRows,
                  }}
                >
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component='div'
          count={filteredUsers ? filteredUsers.length : 0}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Paper>
    </Box>
  );
};

export default AllUsers;
