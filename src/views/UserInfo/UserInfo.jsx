import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import Avatar from '@mui/material/Avatar';
import Typography from '@mui/material/Typography';
import { useContext, useEffect, useState } from 'react';
import Button from '@mui/material/Button';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faPlusCircle } from '@fortawesome/free-solid-svg-icons';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import AppContext from '../../providers/AppContext';
import {
  getDownloadURL,
  ref as storageRef,
  uploadBytes,
} from 'firebase/storage';
import { storage } from '../../configs/firebase.config';
import {
  getLiveUsers,
  updateUserInfo,
  updateUserProfilePicture,
  usersFromSnapshot,
} from '../../services/users.services';

const UserInfo = ({ handle, firstName, avatarURL, lastName, email, phone }) => {
  const { user, userData, setContext } = useContext(AppContext);
  const [users, setUsers] = useState([]);
  const [editable, setEditable] = useState(false);
  const [avatarModalShown, setAvatarModalShown] = useState(false);
  const [file, setFile] = useState(null);
  const [fileDataURL, setFileDataURL] = useState(null);
  const [userInfo, setUserInfo] = useState({
    firstName: firstName,
    lastName: lastName,
    email: email,
    mobile: phone,
  });

  let [errMessage, setErrMessage] = useState('');
  let [editErr, setEditErr] = useState('');

  const imageMimeType = /image\/(png|jpg|jpeg|gif)/i;

  useEffect(()=> {
    getLiveUsers((snapshot) => {
      if(!snapshot.exists()) return null;
  
      setUsers(usersFromSnapshot(snapshot));
    });
  }, [users.length]);

  useEffect(() => {
    let fileReader,
      isCancel = false;
    if (file) {
      fileReader = new FileReader();
      fileReader.onload = (e) => {
        const { result } = e.target;
        if (result && !isCancel) {
          setFileDataURL(result);
        }
      };
      fileReader.readAsDataURL(file);
    }
    return () => {
      isCancel = true;
      if (fileReader && fileReader.readyState === 1) {
        fileReader.abort();
      }
    };
  }, [file]);

  const changeHandler = (e) => {
    const file = e.target?.files[0];
    if (!file.type.match(imageMimeType)) {
      setErrMessage('image file type is not valid');
      return;
    }
    setFile(file);
  };

  const handleModalClose = () => {
    setAvatarModalShown(false);
    setFile(null);
    setFileDataURL(null);
  };

  const uploadAvatar = () => {

    if (!file) {
      return setErrMessage('Please select a file');
    }

    const picture = storageRef(storage, `images/${handle}/avatar`);

    uploadBytes(picture, file)
      .then((snapshot) => {
        return getDownloadURL(snapshot.ref).then((url) => {
          return updateUserProfilePicture(handle, url).then(() => {
            setContext({
              user,
              userData: {
                ...userData,
                avatarURL: url,
              },
            });
          });
        });
      })
      .catch(console.error);
    handleModalClose();
  };

  const handleSubmitInfo = (event) => {
    event.preventDefault();

    if (userInfo.mobile.length !== 10) {
      return setEditErr('Invalid Mobile Number');
    }

    if (users.some((user) => user.mobile === userInfo.mobile && user.handle !== userData.handle)) {
      return setEditErr(`Phone number has already been registered`);
    }

    updateUserInfo(
      handle,
      userInfo.firstName,
      userInfo.lastName,
      userInfo.email,
      userInfo.mobile
    )
      .then(() => {
        setContext({
          user,
          userData: {
            ...userData,
            firstName: userInfo.firstName,
            lastName: userInfo.lastName,
            email: userInfo.email,
            mobile: userInfo.mobile,
          },
        });
      })
      .catch(console.error);
    setEditErr('');

    setEditable(false);
  };

  return (
    <Box ml={10}>
      <Box display={'flex'} alignItems={'center'} mb={4}>
        <Avatar
          component={'button'}
          onClick={() => setAvatarModalShown(true)}
          alt='user avatar'
          sx={{ width: '100px', height: '100px', mr: 5, cursor: 'pointer' }}
          src={
            avatarURL ? avatarURL : `https://robohash.org/${handle}?set=set3`
          }
        />
        <Dialog open={avatarModalShown} onClose={handleModalClose}>
          <DialogTitle>Change Avatar</DialogTitle>
          <DialogContent
            sx={{
              display: 'grid',
              gridTemplateColumn: '1fr 1fr',
              rowGap: '12px',
              alignItems: 'center',
              justifyItems: 'center',
            }}
          >
            <Button
              component='label'
              sx={{ maxWidth: '180px', maxHeight: '40px' }}
            >
              <FontAwesomeIcon icon={faPlusCircle} />
              &nbsp; Add your file
              <input
                onChange={changeHandler}
                type={'file'}
                accept='.jpeg, .jpg, .gif, .png'
                hidden
              />
            </Button>
            {fileDataURL && (
              <Avatar
                sx={{ width: '100px', height: '100px' }}
                src={fileDataURL}
                alt='avatar preview'
              />
            )}
            <DialogContentText sx={{ gridColumn: 'span 2' }}>
              <Typography variant='subtitle1' sx={{ ml: 2 }}>
                Please select a 'JPEG', 'JPG', 'PNG' or 'GIF' file
              </Typography>
            </DialogContentText>
          </DialogContent>
          {errMessage && (
            <span style={{ color: 'red', textAlign: 'center' }}>
              {errMessage}
            </span>
          )}
          <DialogActions>
            <Button onClick={handleModalClose}>Cancel</Button>
            <Button onClick={uploadAvatar}>Upload</Button>
          </DialogActions>
        </Dialog>
        <Typography variant='h4' textTransform={'uppercase'}>
          {`${handle}'s info`}
        </Typography>
      </Box>
      <Box component={'form'} onSubmit={handleSubmitInfo}>
        <Box mb={4}>
          <TextField
            sx={{ mr: 5, minWidth: '300px' }}
            name='firstName'
            variant='standard'
            label='First Name'
            value={userInfo.firstName}
            placeholder='Enter your first name'
            disabled={!editable}
            onChange={(event) =>
              setUserInfo({ ...userInfo, firstName: event.target.value })
            }
            autoComplete='off'
            required
          />
          <TextField
            sx={{ minWidth: '300px' }}
            name='lastName'
            variant='standard'
            label='Last Name'
            value={userInfo.lastName}
            placeholder='Enter your last name'
            disabled={!editable}
            onChange={(event) =>
              setUserInfo({ ...userInfo, lastName: event.target.value })
            }
            autoComplete='off'
            required
          ></TextField>
        </Box>
        <Box mb={4}>
          <TextField
            sx={{ mr: 5, minWidth: '300px' }}
            name='email'
            variant='standard'
            label='Email'
            value={userInfo.email}
            placeholder='Enter your email'
            disabled={!editable}
            onChange={(event) =>
              setUserInfo({ ...userInfo, email: event.target.value })
            }
            autoComplete='off'
            required
          />
          <TextField
            sx={{ minWidth: '300px' }}
            name='mobile'
            variant='standard'
            label='Phone Number'
            value={userInfo.mobile}
            placeholder='Enter your first name'
            disabled={!editable}
            onChange={(event) =>
              setUserInfo({ ...userInfo, mobile: event.target.value })
            }
            autoComplete='off'
            required
          />
        </Box>
        {editErr && (
          <span style={{ color: 'red', display: 'block' }}>{editErr}</span>
        )}
        {editable && <Button type='submit'>Submit</Button>}
      </Box>
      {!editable && (
        <Button onClick={() => setEditable(true)}>
          <FontAwesomeIcon icon={faEdit} /> &nbsp; Edit Info
        </Button>
      )}
    </Box>
  );
};

export default UserInfo;
