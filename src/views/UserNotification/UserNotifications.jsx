import { useContext, useEffect, useState } from 'react';
import AppContext from '../../providers/AppContext';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Tooltip from '@mui/material/Tooltip';
import IconButton from '@mui/material/IconButton';
import DeleteIcon from '@mui/icons-material/Delete';
import moment from 'moment';
import {
  deleteNotification,
  liveUserNotifications,
  notificationsFromSnapshot,
} from '../../services/notification.services';

const headCells = [
  {
    id: 'content',
    numeric: false,
    disablePadding: true,
    label: 'Notification Content',
  },
  {
    id: 'sentOn',
    numeric: true,
    disablePadding: false,
    label: 'Date Received',
  },
  {
    id: 'author',
    numeric: true,
    disablePadding: false,
    label: 'Notification Author',
  },
];

const EnhancedTableHead = () => {
  return (
    <TableHead>
      <TableRow>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={headCell.numeric ? 'right' : 'left'}
            padding='normal'
          >
            {headCell.label}
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
};

const UserNotifications = () => {
  const { userData } = useContext(AppContext);

  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [notifications, setNotifications] = useState(null);

  useEffect(() => {
    return liveUserNotifications(userData?.handle, (snapshot) => {
      if (!snapshot.exists()) {
        setNotifications(null);
      }
      setNotifications(notificationsFromSnapshot(snapshot));
    });
  }, [userData?.handle]);

  const handleChangePage = (_, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleDelete = (messageId) => {
    deleteNotification(userData.handle, messageId);
  };

  // Avoid a layout jump when reaching the last page with empty rows.
  const emptyRows =
    page > 0
      ? Math.max(0, (1 + page) * rowsPerPage - notifications?.length)
      : 0;

  return (
    <Box sx={{ width: '100%' }}>
      <Paper elevation={12} sx={{ width: '100%', mb: 2 }}>
        <Typography sx={{ ml: 2 }} variant='h6' id='tableTitle' component='div'>
          Your Notifications
        </Typography>
        <TableContainer>
          <Table
            sx={{ minWidth: 750 }}
            aria-labelledby='tableTitle'
            size='medium'
          >
            <EnhancedTableHead
              rowCount={notifications ? notifications.length : 0}
            />
            <TableBody>
              {notifications &&
                notifications
                  .slice()
                  .reverse()
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row, index) => {
                    const labelId = `enhanced-table-checkbox-${index}`;

                    return (
                      <TableRow
                        hover
                        role='checkbox'
                        tabIndex={-1}
                        key={row.id}
                      >
                        <TableCell
                          component='th'
                          id={labelId}
                          scope='row'
                          padding='normal'
                          sx={{ width: 600 }}
                        >
                          {row.content}
                        </TableCell>
                        <TableCell align='right'>
                          {
                            <Tooltip title={row.sentOn.toLocaleString()}>
                              <Typography variant='body2' component={'span'}>
                                {moment(row.sentOn).fromNow()}
                              </Typography>
                            </Tooltip>
                          }
                        </TableCell>
                        <TableCell align='right'>{row.author}</TableCell>
                        <TableCell align='right'>
                          {
                            <IconButton onClick={() => handleDelete(row.id)}>
                              <DeleteIcon />
                            </IconButton>
                          }
                        </TableCell>
                      </TableRow>
                    );
                  })}
              {emptyRows > 0 && (
                <TableRow
                  style={{
                    height: 73 * emptyRows,
                  }}
                >
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component='div'
          count={notifications ? notifications.length : 0}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Paper>
    </Box>
  );
};

export default UserNotifications;
