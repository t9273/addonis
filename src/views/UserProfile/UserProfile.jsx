import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import { useContext, useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faUser,
  faUpload,
  faBell,
} from '@fortawesome/free-solid-svg-icons';
import './UserProfile.css';
import AppContext from '../../providers/AppContext';
import TabPanel from '../../components/TabPanel/TabPanel';
import UserInfo from '../UserInfo/UserInfo';
import UserAddonsDashboard from '../UserAddonsDashboard/UserAddonsDashboard';
import UserNotifications from '../UserNotification/UserNotifications';
import { useLocation } from 'react-router-dom';

const tabConfig = () => {
  return {
    display: 'inline-flex',
    justifyContent: 'flex-start',
    flexDirection: 'row',
  };
};

export default function UserProfile() {
  const location = useLocation();

  const { userData } = useContext(AppContext);
  const [tabValue, setTabValue] = useState(
    location?.state?.notifications ? 2 : 0
  );

  const handleChange = (e, newValue) => {
    setTabValue(newValue);
  };

  return (
    <Box maxWidth={1400} mt={3} ml={'auto'} mr={'auto'} position={'relative'}>
      <Typography variant='h4' gutterBottom>
        Hi, {userData?.handle ? userData.handle : 'valuable user'}
      </Typography>
      <Box display={'flex'} flexGrow={1}>
        <Tabs
          orientation='vertical'
          value={tabValue}
          onChange={handleChange}
          sx={{
            borderRight: 1,
            borderColor: 'divider',
            height: 400,
            minHeight: '71vh',
          }}
        >
          <Tab
            sx={tabConfig}
            label={
              <>
                <FontAwesomeIcon className='TabIcon' icon={faUser} />
                User Info
              </>
            }
          />
          <Tab
            sx={tabConfig}
            label={
              <>
                <FontAwesomeIcon className='TabIcon' icon={faUpload} />
                Uploads
              </>
            }
          />
          <Tab
            sx={tabConfig}
            label={
              <>
                <FontAwesomeIcon className='TabIcon' icon={faBell} />
                Notifications
              </>
            }
          />
        </Tabs>
        <TabPanel value={tabValue} index={0}>
          {userData && (
            <UserInfo
              handle={userData.handle}
              firstName={userData?.firstName}
              avatarURL={userData?.avatarURL}
              lastName={userData?.lastName}
              email={userData?.email}
              phone={userData?.mobile}
            />
          )}
        </TabPanel>
        <TabPanel value={tabValue} index={1}>
          {userData && <UserAddonsDashboard handle={userData.handle} />}
        </TabPanel>
        <TabPanel value={tabValue} index={2}>
          <Typography variant='h3'>
            <UserNotifications />
          </Typography>
        </TabPanel>
      </Box>
    </Box>
  );
}
