import { useState, useEffect, useContext } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import { ThemeProvider, useTheme } from '@mui/material/styles';
import Container from '@mui/material/Container';
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import TextField from '@mui/material/TextField';
import Autocomplete from '@mui/material/Autocomplete';
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import Add from '@mui/icons-material/Add';
import { Licenses } from '../../common/Licenses';
import MenuItem from '@mui/material/MenuItem';
import { Tags } from '../../common/Tags';
import InputAdornment from '@mui/material/InputAdornment';
import Avatar from '@mui/material/Avatar';

import {
  getDownloadURL,
  ref as storageRef,
  uploadBytes,
} from 'firebase/storage';
import { storage } from '../../configs/firebase.config';
import {
  editExistingAddon,
  getAddonByName,
  updateAddonPropURL,
  updateAddonState,
} from '../../services/addons.services';
import UploadDialog from '../../components/UploadDialog/UploadDialog';
import AppContext from '../../providers/AppContext';
import Alert from '@mui/material/Alert';
// import { Text } from 'html-react-parser';
import { addUserNotification } from '../../services/notification.services';
import { addOnState } from '../../common/addOnState';
import { IDEs } from '../../common/IDEs';
import { userRole } from '../../common/user-role';

export default function AddonEditView() {
  const navigate = useNavigate();
  const location = useLocation();
  const addonData = location.state.value;
  const ides = Object.keys(IDEs);
  const { userData } = useContext(AppContext);

  const [tags, setTags] = useState(Object.keys(Tags));
  const [initialImage, setInitialImage] = useState(addonData.image);
  const [initialFile, setInitialfile] = useState(addonData.file);

  const [imageDataUrl, setImageDataUrl] = useState(addonData.image);
  const theme = useTheme();
  const imageMimeType = /image\/(png|jpg|jpeg|gif|svg)/i;
  const fileMimeType = /application\/(x-zip-compressed)/i;
  const [openDialog, setOpenDialog] = useState(false);
  let [errMessage, setErrMessage] = useState('');
  const [uploadData, setUploadData] = useState({
    name: addonData.name,
    price: addonData.price,
    file: addonData.file,
    url: addonData.url,
    license: addonData.license,
    downloads: addonData.downloads,
    tags: Object.values(addonData.tags),
    ide: Object.values(addonData.ide),
    author: addonData.author,
    image: '',
    uploadedOn: addonData.uploadedOn,
    state: addonData.state,
    uid: addonData.uid,
  });

  useEffect(() => {
    async function fetchData() {
      try {
        const imgRef = storageRef(storage, uploadData.image);
        const result = await getDownloadURL(imgRef);

        setImageDataUrl(result);
      } catch (error) {
        console.log(error.message);
      }
    }
    fetchData();
  }, [uploadData, imageDataUrl]);

  useEffect(() => {
    async function fetchData() {
      try {
        const fileRef = storageRef(storage, uploadData.file);
        const result = await getDownloadURL(fileRef);

        setUploadData({ ...uploadData, file: result });
      } catch (error) {
        console.log(error.message);
      }
    }
    fetchData();
  }, []);

  useEffect(() => {
    let fileReader,
      isCancel = false;
    if (uploadData.image) {
      fileReader = new FileReader();
      fileReader.onload = (e) => {
        const { result } = e.target;
        if (result && !isCancel) {
          setImageDataUrl(result);
        }
      };
      fileReader.readAsDataURL(uploadData.image);
    }
    return () => {
      isCancel = true;
      if (fileReader && fileReader.readyState === 1) {
        fileReader.abort();
      }
    };
  }, [uploadData.image]);

  const imgChangeHandler = (e) => {
    const file = e.target?.files[0];
    if (!file.type.match(imageMimeType)) {
      setErrMessage('image file type is not valid');
      return;
    }

    setUploadData({ ...uploadData, image: file });
  };

  const fileChangeHandler = (e) => {
    const file = e.target?.files[0];
    if (!file.type.match(fileMimeType)) {
      setErrMessage((prev) => prev + '\n file type is not valid');
      return;
    } else {
      setErrMessage('');
    }

    setUploadData({ ...uploadData, file: file });
  };

  const handleEdit = (e) => {
    e.preventDefault();

    if (!uploadData.file) {
      setErrMessage('Please choose a file to upload');
      return;
    }

    if (!uploadData.name) {
      setErrMessage('You must provide a name for your Addon');
      return;
    }

    if (!uploadData.url) {
      setErrMessage(
        'You must provide an online location address for your Addon'
      );
      return;
    }

    if (!uploadData.ide) {
      setErrMessage('Your Addon should have IDE selected');
      return;
    }

    editExistingAddon(
      uploadData.uid,
      uploadData.name,
      uploadData.license,
      uploadData.url,
      uploadData.tags,
      uploadData.ide,
      +uploadData.price === 0 ? 'FREE' : +uploadData.price
    );

    if (initialImage !== imageDataUrl) {
      getAddonByName(uploadData.name)
        .then((res) => {
          const imageStoreRef = storageRef(
            storage,
            `images/addOns/${res.uid}/${uploadData.image.name}`
          );
          return uploadBytes(imageStoreRef, uploadData.image).then(
            (snapshot) => {
              return getDownloadURL(snapshot.ref).then((url) => {
                return updateAddonPropURL(res.uid, 'image', url);
              });
            }
          );
        })
        .catch((e) => console.log(e.message));
    }

    if (initialFile !== uploadData.file) {
      getAddonByName(uploadData.name)
        .then((res) => {
          const addOnStoreRef = storageRef(
            storage,
            `addOns/${res.uid}/${uploadData.file.name}`
          );
          return uploadBytes(addOnStoreRef, uploadData.file).then(
            (snapshot) => {
              return getDownloadURL(snapshot.ref).then((url) => {
                return updateAddonPropURL(res.uid, 'file', url);
              });
            }
          );
        })
        .catch((e) => console.log(e.message));
    }

    if (userData.role === userRole.ADMIN) {
      updateAddonState(uploadData.uid, addOnState.APPROVED);
      addUserNotification(
        uploadData.author,
        `${uploadData.name} has been approved by ${userData.handle} / admin`,
        `system`
      );
      return navigate('../dashboard');
    }

    navigate(`../home`);
  };

  const handleChange = (e, prop) => {
    setUploadData({ ...uploadData, [prop]: e.target.value || '' });
  };

  const handleTags = (value) => {
    setUploadData({
      ...uploadData,
      tags: typeof value === 'string' ? value.split(',') : value,
    });
  };

  const handleCancel = () => {
    navigate(`../addons/${addonData.name}`);
  };

  return (
    <div>
      {(userData?.role === userRole.ADMIN ||
        userData?.handle === uploadData.author) && (
        <ThemeProvider theme={theme}>
          <CssBaseline />
          {openDialog && (
            <UploadDialog
              navigate={navigate}
              openDialog={openDialog}
              setOpenDialog={setOpenDialog}
            />
          )}
          <Container component='main' maxWidth='xs'>
            <Box
              sx={{
                marginTop: 8,
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
              }}
            >
              <Typography component='h1' variant='h5'>
                Edit Addon data
              </Typography>
              <Box
                component='form'
                noValidate
                onSubmit={handleEdit}
                sx={{ marginTop: 3 }}
              >
                <Grid container spacing={2} margin={2}>
                  <Grid item xs={12} sm={6} sx={{ width: '60vw' }}>
                    <TextField
                      name='addOnName'
                      required
                      autoComplete='none'
                      fullWidth
                      id='addOnName'
                      label='AddOn Name'
                      onChange={(e) => {
                        handleChange(e, 'name');
                        setErrMessage('');
                      }}
                      value={uploadData.name}
                      error={uploadData.name === ''}
                      helperText={
                        uploadData.name === '' ? 'Please enter name' : ' '
                      }
                      autoFocus
                    />
                  </Grid>
                  <Grid item xs={12} sm={6} sx={{ width: '10vw' }}>
                    <TextField
                      required
                      label='Price'
                      autoFocus
                      type='number'
                      value={uploadData.price}
                      fullWidth
                      InputProps={{
                        startAdornment: (
                          <InputAdornment position='start'>$</InputAdornment>
                        ),
                      }}
                      onChange={(e) => handleChange(e, 'price')}
                    />
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <TextField
                      id='license'
                      select
                      label='License'
                      value={uploadData.license}
                      helperText='Please select your License'
                      onChange={(e) => handleChange(e, 'license')}
                      fullWidth
                    >
                      {Object.keys(Licenses).map((license) => (
                        <MenuItem key={license} value={license}>
                          {license}
                        </MenuItem>
                      ))}
                    </TextField>
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <TextField
                      required={true}
                      fullWidth
                      autoComplete='none'
                      error={uploadData.url === ''}
                      helperText={
                        uploadData.url === ''
                          ? 'Please enter provide a link for online addon storage'
                          : ''
                      }
                      value={uploadData.url}
                      onChange={(e) => {
                        handleChange(e, 'url');
                        setErrMessage('');
                      }}
                      label='http://'
                      id='http'
                    ></TextField>
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <Stack spacing={3}>
                      <Autocomplete
                        multiple
                        id='tags-outlined'
                        options={tags}
                        value={uploadData.tags}
                        onChange={(event, value) => handleTags(value)}
                        filterSelectedOptions
                        renderInput={(params) => (
                          <TextField
                            {...params}
                            label='Select Tag'
                            placeholder='Select Tag'
                          />
                        )}
                      />
                    </Stack>
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <Stack spacing={3}>
                      <Autocomplete
                        id='ides-outlined'
                        multiple
                        value={uploadData.ide}
                        options={ides}
                        onChange={(event, value) => {
                          setUploadData({ ...uploadData, ide: value });
                          setErrMessage('');
                        }}
                        filterSelectedOptions
                        renderInput={(params) => (
                          <TextField
                            error={!uploadData.ide}
                            helperText={
                              uploadData.ide === '' ? 'Please choose IDE' : ''
                            }
                            {...params}
                            label='Selected IDE'
                            placeholder='Select IDE'
                          />
                        )}
                      />
                    </Stack>
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <Button
                      component='label'
                      startIcon={<Add />}
                      sx={{ maxWidth: '180px', maxHeight: '40px' }}
                    >
                      {' '}
                      Add File Image
                      <input
                        onChange={imgChangeHandler}
                        type={'file'}
                        accept='.jpeg, .jpg, .gif, .png, .svg'
                        hidden
                      />
                    </Button>
                    {imageDataUrl && (
                      <Avatar
                        sx={{ width: '100px', height: '100px' }}
                        src={imageDataUrl}
                        alt='avatar preview'
                      />
                    )}
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <Button
                      component='label'
                      startIcon={<Add />}
                      sx={{ maxWidth: '180px', maxHeight: '40px' }}
                    >
                      {' '}
                      Add File
                      <input
                        onChange={fileChangeHandler}
                        type={'file'}
                        accept='.zip, .jar'
                        hidden
                      />
                    </Button>
                    <TextField
                      fullWidth
                      value={
                        uploadData?.file?.name
                          ? uploadData?.file?.name
                          : uploadData?.file
                      }
                      error={uploadData.file === ''}
                      helperText={
                        uploadData.file === ''
                          ? 'Please select a file to upload'
                          : ''
                      }
                      disabled
                    />
                  </Grid>
                  <Grid item xs={4}>
                    <Button
                      type='submit'
                      onClick={handleEdit}
                      variant='outlined'
                    >
                      {userData.role === userRole.ADMIN ? 'Approve' : 'Save'}
                    </Button>
                    <Button
                      sx={{ ml: 2 }}
                      type='submit'
                      onClick={handleCancel}
                      variant='outlined'
                    >
                      Cancel
                    </Button>
                  </Grid>
                  {errMessage && <Alert severity='error'>{errMessage}</Alert>}
                </Grid>
              </Box>
            </Box>
          </Container>
        </ThemeProvider>
      )}
    </div>
  );
}
