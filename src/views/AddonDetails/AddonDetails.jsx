import parse from 'html-react-parser';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import Tooltip from '@mui/material/Tooltip';
import Stack from '@mui/material/Stack';
import './AddonDetails.css';
import Button from '@mui/material/Button';
import Modal from '@mui/material/Modal';
import TextField from '@mui/material/TextField';
import Link from '@mui/material/Link';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faCodePullRequest,
  faCodeBranch,
  faCodeCommit,
  faCircleExclamation,
  faStar,
} from '@fortawesome/free-solid-svg-icons';
import { useParams, useNavigate } from 'react-router-dom';
import { useQuery } from 'react-query';
import {
  addToFeatured,
  getAddonByName,
  recordPurchase,
  removeFromFeatured,
  updateAddonState,
  updateDownloads,
  updateRating,
} from '../../services/addons.services';
import { useContext, useState } from 'react';
import AppContext from '../../providers/AppContext';
import moment from 'moment';
import {
  fetchBranches,
  fetchGithubData,
  fetchPulls,
  fetchReadme,
} from '../../services/gitHub.services';
import { addOnState } from '../../common/addOnState';
import { userRole } from '../../common/user-role';
import { addUserNotification } from '../../services/notification.services';
import AddonChart from '../../components/Charts/AddonChart';
import { getAdmins, usersFromSnapshot } from '../../services/users.services';
import AddonDetailsHeader from '../../components/AddonDetails/AddonDetailsHeader';
import { constants } from '../../common/constants';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};

export default function AddonDetails() {
  const { userData } = useContext(AppContext);
  const { addonName } = useParams();
  const navigate = useNavigate();

  const [open, setOpen] = useState(false);
  const [openReport, setOpenReport] = useState(false);
  let [value, setValue] = useState('');
  let [errMessage, setErrMessage] = useState('');
  const [downloadCount, setDownloadCount] = useState(0);
  const [rating, setRating] = useState(0);
  const [isPurchased, setIsPurchased] = useState(false);
  const [featured, setFeatured] = useState();
  const [approveBtn, setApproveBtn] = useState();

  const { data: addonData } = useQuery(
    ['addonData', addonName],
    () =>
      getAddonByName(addonName)
        .then((response) => {
          setDownloadCount(Object.keys(response.downloads).length);
          setIsPurchased(response.purchasedBy.includes(userData?.handle));
          setFeatured(response.featured);
          response.state !== addOnState.APPROVED
            ? setApproveBtn(true)
            : setApproveBtn(false);
          return response;
        })
        .catch((e) => console.log(e.message)),
    { cacheTime: 10000 }
  );

  const repoPath = addonData?.url.replace('https://github.com/', '');

  const { data: readme } = useQuery(
    ['readme', repoPath],
    () => fetchReadme(repoPath),
    {
      enabled: !!repoPath,
    }
  );

  const { data: githubData } = useQuery(
    ['githubData', repoPath],
    () => fetchGithubData(repoPath),
    {
      enabled: !!repoPath,
    }
  );

  const { data: branches } = useQuery(
    ['branches', repoPath],
    () => fetchBranches(repoPath),
    {
      enabled: !!repoPath,
    }
  );

  const { data: pulls } = useQuery(
    ['pulls', repoPath],
    () => fetchPulls(repoPath),
    { enabled: !!repoPath }
  );

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setValue('');
    setErrMessage('');
    setOpen(false);
  };

  const handleOpenReport = () => {
    setOpenReport(true);
  };

  const handleCloseReport = () => {
    setValue('');
    setErrMessage('');
    setOpenReport(false);
  };

  const handleChange = (event) => {
    setValue(event.target.value);
  };

  const handleDownload = () => {
    if (!Object.keys(addonData.downloads).includes(userData.handle)) {
      setDownloadCount(downloadCount + 1);
    }
    updateDownloads(addonData.uid, userData.handle);
  };

  const handleRating = (_, newValue) => {
    setRating(newValue);
    updateRating(addonData.uid, userData.handle, newValue);
  };

  const handleEdit = (value) => {
    navigate('../edit-addon', { state: { value } });
  };

  const handlePurchase = () => {
    setIsPurchased(true);
    recordPurchase(addonData.uid, userData.handle);
  };

  const handleUpdateAddonState = (addonId, addonState) => {
    updateAddonState(addonId, addonState);

    if (addonState === addOnState.APPROVED) {
      addUserNotification(
        addonData.author,
        `${addonData.name} has been approved by ${userData.handle} / admin`,
        `system`
      );
      setApproveBtn(false);
    }
  };

  const handleReject = () => {
    if (value.length === 0) {
      return setErrMessage('Please enter a reason for rejection');
    }

    addUserNotification(
      addonData.author,
      `${addonData.name} has been rejected because: ${value}`,
      `${userData.handle} / admin`
    );

    updateAddonState(addonData.uid, addOnState.REJECTED);

    handleClose();
  };

  const handleReport = async () => {
    if (value.length === 0) {
      return setErrMessage('Please enter a reason for report');
    }
    const adminsSnapshot = await getAdmins();

    const admins = usersFromSnapshot(adminsSnapshot).reduce((acc, admin) => {
      acc.push(admin.handle);
      return acc;
    }, []);

    const usersToNotify = new Set([...admins, addonData.author]);

    usersToNotify.forEach((user) => {
      addUserNotification(
        user,
        `${addonData.name} has been reported because: ${value}`,
        `${userData.handle}`
      );
    });

    handleCloseReport();
  };

  const handleAddToFeatured = (addon) => {
    setFeatured(true);
    return addToFeatured(addon.uid);
  };

  const handleRemoveFromFeatured = (addon) => {
    setFeatured(false);
    return removeFromFeatured(addon.uid);
  };

  return !readme ? (
    <Box textAlign={'center'} minHeight={'76vh'} width={'80vw'}>
      <img
        src={constants.SPINNER_URL}
        alt='loading spinner'
        style={{ margin: '2em' }}
      />
    </Box>
  ) : (
    githubData && pulls && branches && (
      <Box>
        <Box p={5} pb={2} mt={1} sx={{ backgroundColor: 'gainsboro' }}>
          <AddonDetailsHeader
            addonData={addonData}
            rating={rating}
            handleDownload={handleDownload}
            handlePurchase={handlePurchase}
            handleRating={handleRating}
            description={githubData.description}
            purchasedBy={addonData.purchasedBy}
            isPurchased={isPurchased}
            key='header'
          />
        </Box>
        <Grid container mt={4} mb={4}>
          <Grid item xs={9}>
            <Box pl={5} ml={'auto'} mr={'auto'}>
              {readme &&
                parse(readme, {
                  replace: ({ attribs, name }) => {
                    if (attribs?.class === 'anchor') {
                      return <></>;
                    }

                    if (name === 'a' && !attribs.href.startsWith('http')) {
                      attribs.href = null;
                    }

                    if (name === 'a') {
                      attribs.target = '_blank';
                      attribs.rel = 'noreferrer';
                    }

                    if (name === 'img' && !attribs.src.startsWith('http')) {
                      return (
                        <img
                          alt='github asset'
                          src={`${addonData.url}/blob/${githubData.default_branch}/${attribs.src}?raw=true`}
                          style={{ maxWidth: '100%' }}
                        />
                      );
                    }
                  },
                })}
            </Box>
          </Grid>
          <Grid item xs={3}>
            <Grid pl={5} container spacing={3} flexDirection='column'>
              <Grid item>
                <Typography mb={1} variant='subtitle1'>
                  Tags
                </Typography>

                {addonData.tags.map((tag, index) => (
                  <Button
                    href={`/addons?tag=${tag}`}
                    key={index}
                    sx={{ margin: '3px' }}
                    variant='outlined'
                    size='small'
                  >
                    {tag}
                  </Button>
                ))}
              </Grid>
              <Grid item>
                <Typography variant='subtitle1'>Supported IDEs</Typography>
                {addonData.ide.map((ide, index) => (
                  <Button
                    href={`/addons?ide=${ide}`}
                    key={index}
                    size='small'
                    variant='outlined'
                    sx={{ m: '3px', mt: '8px' }}
                  >
                    {ide}
                  </Button>
                ))}
              </Grid>
              <Grid item>
                <Typography variant='subtitle1' mt={1}>
                  Links
                </Typography>
                <Stack spacing={1} maxWidth={20}>
                  {addonData.homepage && (
                    <Link
                      href={addonData.homepage}
                      size='small'
                      target={'_blank'}
                      rel='noreferrer'
                      sx={{ cursor: 'pointer' }}
                    >
                      Home Page
                    </Link>
                  )}
                  <Link
                    href={addonData.url}
                    size='small'
                    target={'_blank'}
                    rel='noreferrer'
                    sx={{ cursor: 'pointer' }}
                  >
                    Repo
                  </Link>
                  <Link
                    href={`${addonData.url}/issues`}
                    target='_blank'
                    rel='noreferrer'
                    size='small'
                    sx={{ cursor: 'pointer' }}
                  >
                    Issues
                  </Link>
                  <Link
                    href={`${addonData.url}/commits`}
                    target='_blank'
                    rel='noreferrer'
                    size='small'
                    sx={{ cursor: 'pointer' }}
                  >
                    Commits
                  </Link>
                </Stack>
              </Grid>
              <Grid item>
                <Typography mb={1} variant='subtitle1'>
                  GitHub Info
                </Typography>
                <Stack spacing={1}>
                  <Typography variant='caption'>
                    <FontAwesomeIcon icon={faCodePullRequest} width='12px' />{' '}
                    {pulls.length} Pull Requests
                  </Typography>
                  <Typography variant='caption'>
                    <FontAwesomeIcon icon={faCodeBranch} width='12px' />{' '}
                    {branches.length} Branches
                  </Typography>
                  <Tooltip title={moment(githubData.pushed_at).format('lll')}>
                    <Typography variant='caption'>
                      <FontAwesomeIcon icon={faCodeCommit} width='12px' /> Last
                      Commit: {moment(githubData.pushed_at).fromNow()}
                    </Typography>
                  </Tooltip>
                  <Typography variant='caption'>
                    <FontAwesomeIcon icon={faCircleExclamation} width='12px' />{' '}
                    {githubData.open_issues_count} Open Issues
                  </Typography>
                  <Typography variant='caption'>
                    <FontAwesomeIcon icon={faStar} width='12px' />{' '}
                    {githubData.stargazers_count} GitHub Stars
                  </Typography>
                </Stack>
              </Grid>

              <Grid item sx={{ maxWidth: '100%' }}>
                <Typography variant='subtitle1' mt={1}>
                  Monthly Stats
                </Typography>
                <AddonChart addonData={addonData} />
              </Grid>

              {userData?.handle &&
                userData?.role !== userRole.BANNED &&
                userData?.role !== userRole.ADMIN &&
                userData?.role !== userRole.DELETED && (
                  <Grid item>
                    <Grid item>
                      <Button
                        onClick={handleOpenReport}
                        size='small'
                        color='error'
                      >
                        Report Abuse
                      </Button>
                      <Modal
                        open={openReport}
                        onClose={handleCloseReport}
                        aria-labelledby='modal-modal-title'
                        aria-describedby='modal-modal-description'
                      >
                        <Box sx={style}>
                          <Typography
                            id='modal-modal-title'
                            variant='h6'
                            component='h2'
                          >
                            Enter a reason for Report
                          </Typography>
                          <Box
                            component='form'
                            fullWidth
                            sx={{
                              '& .MuiTextField-root': { m: 1 },
                            }}
                            noValidate
                            autoComplete='off'
                          >
                            <div>
                              <TextField
                                id='outlined-multiline-static'
                                required
                                label='Reason'
                                multiline
                                fullWidth
                                rows={4}
                                value={value}
                                onChange={handleChange}
                              />
                            </div>
                            {errMessage && (
                              <span style={{ color: 'red', display: 'block' }}>
                                {errMessage}
                              </span>
                            )}
                            <Button
                              onClick={handleReport}
                              size='small'
                              color='error'
                            >
                              Send Report
                            </Button>
                            <Button onClick={handleCloseReport} size='small'>
                              Cancel
                            </Button>
                          </Box>
                        </Box>
                      </Modal>
                    </Grid>
                  </Grid>
                )}
              {userData?.role === userRole.ADMIN && (
                <Grid item>
                  {featured ? (
                    <Button
                      onClick={() => handleRemoveFromFeatured(addonData)}
                      size='small'
                    >
                      Remove from Featured
                    </Button>
                  ) : (
                    <Button
                      onClick={() => handleAddToFeatured(addonData)}
                      size='small'
                    >
                      Add to Featured
                    </Button>
                  )}
                </Grid>
              )}

              <Grid item>
                {(userData?.role === userRole.ADMIN ||
                  userData?.handle === addonData.author) && (
                  <Button onClick={() => handleEdit(addonData)} size='small'>
                    Edit Addon
                  </Button>
                )}
              </Grid>
              <Grid item>
                {userData?.role === userRole.ADMIN && (
                  <Button
                    onClick={handleDownload}
                    href={addonData.file}
                    size='small'
                  >
                    Download
                  </Button>
                )}
              </Grid>
              {userData?.role === userRole.ADMIN &&
                addonData.state !== addOnState.APPROVED && (
                  <Grid item>
                    {approveBtn && (
                      <Button
                        onClick={() =>
                          handleUpdateAddonState(
                            addonData.uid,
                            addOnState.APPROVED
                          )
                        }
                        size='small'
                        color='success'
                      >
                        Approve
                      </Button>
                    )}
                  </Grid>
                )}
              {userData?.role === userRole.ADMIN && (
                <Grid item>
                  <Button onClick={handleOpen} size='small' color='error'>
                    Reject
                  </Button>
                  <Modal
                    open={open}
                    onClose={handleClose}
                    aria-labelledby='modal-modal-title'
                    aria-describedby='modal-modal-description'
                  >
                    <Box sx={style}>
                      <Typography
                        id='modal-modal-title'
                        variant='h6'
                        component='h2'
                      >
                        Enter a reason for Rejection
                      </Typography>
                      <Box
                        component='form'
                        fullWidth
                        sx={{
                          '& .MuiTextField-root': { m: 1 },
                        }}
                        noValidate
                        autoComplete='off'
                      >
                        <div>
                          <TextField
                            id='outlined-multiline-static'
                            required
                            label='Reason'
                            multiline
                            fullWidth
                            rows={4}
                            value={value}
                            onChange={handleChange}
                          />
                        </div>
                        {errMessage && (
                          <span style={{ color: 'red', display: 'block' }}>
                            {errMessage}
                          </span>
                        )}
                        <Button
                          onClick={handleReject}
                          size='small'
                          color='error'
                        >
                          Reject
                        </Button>
                        <Button onClick={handleClose} size='small'>
                          Cancel
                        </Button>
                      </Box>
                    </Box>
                  </Modal>
                </Grid>
              )}
            </Grid>
          </Grid>
        </Grid>
      </Box>
    )
  );
}
