import { useEffect, useState } from 'react';
import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom';
import './App.css';
import AppContext from './providers/AppContext';
import Footer from './components/Footer/Footer';
import NavigationBar from './views/NavigationBar/NavigationBar';
import Container from '@mui/material/Container';
import Home from './views/Home/Home';
import Upload from './views/Upload/Upload';
import Authenticated from './hoc/Authenticated';
import { useAuthState } from 'react-firebase-hooks/auth';
import { auth } from './configs/firebase.config';
import { getUserData } from './services/users.services';
import Addons from './views/Addons/Addons';
import SignIn from './views/SignIn/SignIn';
import SignUp from './views/SignUp/SignUp';
import AddonDetails from './views/AddonDetails/AddonDetails';
import UserProfile from './views/UserProfile/UserProfile';
import About from './views/About/About';

import { PayPalScriptProvider } from '@paypal/react-paypal-js';
import { paypalOptions } from './configs/paypal.config';

import AdminDashboard from './views/AdminDashboard/AdminDashboard';
import { QueryClient, QueryClientProvider } from 'react-query';
import AddonEditView from './views/AddonEditView/AddonEditView';
import AddonChart from './components/Charts/AddonChart';
import Donate from './views/Donate/Donate';
import IsDeletedUser from './hoc/isDeletedUser';
import NotFound from './components/PageNotFound/PageNotFound';

function App() {
  const queryClient = new QueryClient();

  const [appState, setAppState] = useState({
    user: null,
    userData: null,
  });

  const [user] = useAuthState(auth);

  useEffect(() => {
    if (user === null) return;

    getUserData(user.uid)
      .then((snapshot) => {
        if (!snapshot.exists()) {
          throw new Error('Something went wrong!');
        }

        setAppState({
          user,
          userData: snapshot.val()[Object.keys(snapshot.val())[0]],
        });
      })
      .catch((e) => alert(e.message));
  }, [user]);

  return (
    <BrowserRouter>
      <AppContext.Provider value={{ ...appState, setContext: setAppState }}>
        <QueryClientProvider client={queryClient}>
          <IsDeletedUser>
            <PayPalScriptProvider options={paypalOptions}>
              <NavigationBar />
              <Container
                maxWidth='xl'
                sx={{ minHeight: 'calc(100vh - 142px)' }}
              >
                <Routes>
                  <Route index element={<Navigate replace to='home' />} />
                  <Route path='home' element={<Home />} />
                  <Route path='signIn' element={<SignIn />} />
                  <Route path='signUp' element={<SignUp />} />
                  <Route path='addons' element={<Addons />} />
                  <Route path='dashboard' element={<AdminDashboard />} />
                  <Route path='addons/:addonName' element={<AddonDetails />} />
                  <Route path='/users/:handle' element={<UserProfile />} />
                  <Route path='edit-addon' element={<AddonEditView />} />
                  <Route path='chart' element={<AddonChart />} />
                  <Route path='about' element={<About />} />
                  <Route
                    path='/upload'
                    element={
                      <Authenticated>
                        <Upload />
                      </Authenticated>
                    }
                  />
                  <Route path='donate' element={<Donate />} />
                  <Route path="*" element={<NotFound />} />
                </Routes>
              </Container>
              <Footer />
            </PayPalScriptProvider>
          </IsDeletedUser>
        </QueryClientProvider>
      </AppContext.Provider>
    </BrowserRouter>
  );
}

export default App;
