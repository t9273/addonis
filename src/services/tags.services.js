import { ref, push, get, query, equalTo, orderByChild, update, onValue } from 'firebase/database';
import { db } from '../configs/firebase.config'


export const getTagsFromSnapshot = (snapshot) => {

  const tagSnapshot = snapshot.val();
  return Object.keys(tagSnapshot);
}

export const getTags = () => {
  return get(ref(db, 'tags'))
  .then(snapshot => {
    return snapshot.val()
  })
  .catch(e => console.log(e))
}

export const addTag = (tagName) => {
  const updates = {};

  updates[`tags/${tagName}`] = true;
  return update(ref(db), updates);
}

//Add tags from an object of multiple tags
export const AddTags = (tags) => {
  const tagArr = Object.keys(tags);
  return tagArr.forEach(tag => update(ref(db), {
    [`tags/${tag}`] : true,
  }))

};