import { onValue, push, ref } from 'firebase/database';
import { db } from '../configs/firebase.config';

export const donationsFromSnapshot = (snapshot) => {
  if (!snapshot.exists()) {
    return [];
  }

  const donations = snapshot.val();
  return Object.keys(donations).map((id) => {
    const donation = donations[id];

    return { ...donation, createdOn: new Date(donation.createdOn), id };
  });
};
export const liveDonations = (listen) => {
  return onValue(ref(db, 'donations'), listen);
};

export const recordDonation = (handle, value) => {
  return push(ref(db, 'donations'), {
    donatedBy: handle,
    amount: value,
    createdOn: new Date().valueOf(),
  });
};
