import { get, set, ref, query, equalTo, orderByChild, update, onValue, remove } from 'firebase/database';
import { db, storage } from '../configs/firebase.config';
import { userRole } from '../common/user-role';


export const getUserFromSnapshot = (snapshot) => {
  const userSnapshot = snapshot.val();
  return userSnapshot;
}

export const usersFromSnapshot = (snapshot) => {
    const userSnapshot = snapshot.val();    
    return Object.keys(userSnapshot)
      .map((userId) => {
        const user = userSnapshot[userId];
  
        return {
          ...user,
          uid: userId,
          handle: user.handle,
          email: user.email,
          mobile: user.mobile,
          role: user.role,
        };
      });
}

export const usersHandleFromSnapshot = (snapshot) => {
  const userSnapshot = snapshot.val();    
  return Object.keys(userSnapshot)
    .map((userId) => {
      const user = userSnapshot[userId];

      return user.handle
    });
};

export const getUserByHandle = (handle) => {
  return get(ref(db, `users/${handle}`))
};

export const createUserHandle = (handle, uid, mobile, email, verified) => {

  return set(ref(db, `users/${handle}`), { handle,
    uid,
    mobile,
    email,
    createdOn: new Date().toLocaleDateString(),
    verifiedEmail: verified,
    role: userRole.ORDINARY });
};

export const getUserData = (uid) => {

  return get(query(ref(db, 'users'), orderByChild('uid'), equalTo(uid)));
};

export const getAdmins = () => {
  return get(query(ref(db, 'users'), orderByChild('role'), equalTo(userRole.ADMIN)));
};

export const getAllUsersData = () => {
  return get(query(ref(db, 'users')))
}

export const updateUserRole = (handle, role) => {
  return update(ref(db), {
    [`users/${handle}/role`]: role,
  });
};

export const getLiveUsers = (listen) => {
  return onValue(ref(db, 'users'), listen);
};

export const updateUserInfo = (handle, firstName, lastName, email, mobile) => {
  return update(ref(db), {
    [`users/${handle}/firstName`]: firstName,
    [`users/${handle}/lastName`]: lastName,
    [`users/${handle}/email`]: email,
    [`users/${handle}/mobile`]: mobile,
  });
};

export const verifiedMail = (handle, status) => {
  return update(ref(db), {
    [`users/${handle}/verifiedEmail`]: status,
  });
}

export const updateUserProfilePicture = (handle, url) => {
  return update(ref(db), {
    [`users/${handle}/avatarURL`]: url,
  });
};

export const addToUserAddonList = (handle, uid, addOnId) => {
return getUserData(uid)
  .then(snapshot => {
    const addons = snapshot.val()[`${handle}`].addOns;
    return update(ref(db), {
      [`users/${handle}/addOns/`]: {...addons, [addOnId] : true}
    })
  })
  .catch(e => console.log(e));
}
export const deleteUser = (handle, userImg) => {

  update(ref(db), {
    [`users/${handle}/role`]: userRole.DELETED,
  });
};
