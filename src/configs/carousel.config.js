export const carouselConfig = {
  showDots: true,
  responsive: {
    desktop: {
      breakpoint: { max: 3000, min: 1200 },
      items: 4,
      slidesToSlide: 4,
    },
    laptop: {
      breakpoint: { max: 1200, min: 820 },
      items: 3,
      slidesToSlide: 3,
    },
    tablet: {
      breakpoint: { max: 820, min: 464 },
      items: 2,
      slidesToSlide: 2,
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1,
      slidesToSlide: 1,
    },
  },
  infinite: true,
  shouldResetAutoplay: false,
  autoPlay: false,
  keyBoardControl: false,
  transitionDuration: 500,
  containerClass: 'CarouselContainer',
  removeArrowOnDeviceType: ['tablet', 'mobile'],
  dotListClass: 'custom-dot-list-style',
  itemClass: 'CarouselCard',
};
