import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faFaceGrinBeamSweat,
  faHeart,
} from '@fortawesome/free-solid-svg-icons';
import './Footer.css';
import { NavLink } from 'react-router-dom';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';

export default function Footer() {
  return (
    <Box className='Footer' display={'flex'} justifyContent={'space-around'}>
      <Box>
        <Typography className='attribution' variant='body1'>
          Made with lots of &nbsp;
          <span className='sweat-icon'>
            <FontAwesomeIcon icon={faFaceGrinBeamSweat} />
          </span>
          &nbsp; and &nbsp;
          <span className='heart-icon'>
            <FontAwesomeIcon icon={faHeart} />
          </span>
          &nbsp; by <NavLink to={'/about'}>The Binary Three Dudes</NavLink>
        </Typography>
      </Box>
      <Box display={'flex'}>
        <Typography mr={3} variant='body1'>
          Support us by <NavLink to='/donate'>donating</NavLink>
        </Typography>
      </Box>
    </Box>
  );
}
