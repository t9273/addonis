import './Header.css';

export default function Header() {
  return (
    <div className='Header'>
      <h1 className='logo'>BTD Addons</h1>
    </div>
  );
}
