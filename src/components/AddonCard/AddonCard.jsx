import Grid from '@mui/material/Grid';
import Card from '@mui/material/Card';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActionArea from '@mui/material/CardActionArea';
import Typography from '@mui/material/Typography';
import Rating from '@mui/material/Rating';
import Tooltip from '@mui/material/Tooltip';
import PropTypes from 'prop-types';

const AddonCard = ({ imgSrc, title, author, downloads, rating, price }) => {
  return (
    <Card
      raised
      sx={{
        maxWidth: 200,
        borderRadius: 2,
        height: 200,
        display: 'flex',
        alignContent: 'space-between',
      }}
    >
      <CardActionArea href={`/addons/${title}`}>
        <CardMedia
          component='img'
          height='70'
          src={imgSrc}
          sx={{
            maxWidth: 80,
            marginLeft: 'auto',
            marginRight: 'auto',
            objectFit: 'contain',
          }}
        />
        <CardContent>
          <Tooltip title={title}>
            <Typography noWrap gutterBottom variant='h6' textAlign={'center'}>
              {title}
            </Typography>
          </Tooltip>

          <Grid container mb={1} justifyContent={'space-between'}>
            <Grid item xs={8} sx={{ wordWrap: 'break-word' }}>
              <Typography
                variant='caption'
                color='text.secondary'
                maxWidth={110}
              >
                {author}
              </Typography>
            </Grid>
            <Grid item>
              <Typography
                variant='caption'
                color='text.secondary'
                component='span'
              >
                📩 {downloads}
              </Typography>
            </Grid>
          </Grid>
          <Grid container justifyContent={'space-between'}>
            <Grid item>
              <Tooltip title={`Average rating: ${rating ? rating : 0}`}>
                <Typography variant='body2' component='span'>
                  <Rating
                    readOnly
                    size='small'
                    value={rating}
                    precision={0.1}
                    sx={{ color: 'darkred' }}
                  />
                </Typography>
              </Tooltip>
            </Grid>
            <Grid item>
              <Typography variant='body2' component='span'>
                {price !== 'FREE' ? `$${price}` : price}
              </Typography>
            </Grid>
          </Grid>
        </CardContent>
      </CardActionArea>
    </Card>
  );
};

AddonCard.propTypes = {
  imgSrc: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  author: PropTypes.string.isRequired,
  downloads: PropTypes.number.isRequired,
  rating: PropTypes.number.isRequired,
  price: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
};

export default AddonCard;
