import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import Tooltip from '@mui/material/Tooltip';
import Button from '@mui/material/Button';
import Rating from '@mui/material/Rating';
import { useContext } from 'react';
import AppContext from '../../providers/AppContext';
import { PayPalButtons } from '@paypal/react-paypal-js';
import { constants } from '../../common/constants';

export default function AddonDetailsHeader({
  addonData,
  rating,
  handleRating,
  description,
  purchasedBy,
  isPurchased,
  handleDownload,
  handlePurchase,
}) {
  const { userData } = useContext(AppContext);

  const purchasedByUser = isPurchased
    ? isPurchased
    : purchasedBy.includes(userData?.handle);

  return (
    <Grid container spacing={3}>
      <Grid item xs={1}>
        <img
          className='AddonLogo'
          alt='addon logo'
          src={addonData.image ? addonData.image : constants.DEFAULT_LOGO_URL}
        ></img>
      </Grid>
      <Grid item xs={11}>
        <Grid container flexDirection={'column'}>
          <Grid item maxWidth={500}>
            <Typography variant='h4'>{addonData.name}</Typography>
            <Box
              display={'flex'}
              flexDirection={'row'}
              alignItems={'center'}
              justifyContent={'flex-start'}
            >
              <Typography variant='body1' p={1}>
                {addonData.author}
              </Typography>
              <Divider
                sx={{
                  height: '20px',
                  margin: 'auto',
                  borderRight: '2px solid',
                }}
                orientation='vertical'
                flexItem
              />
              <Typography variant='body2' p={1}>
                {addonData.downloadCount} Downloads
              </Typography>
              <Divider
                sx={{
                  height: '20px',
                  margin: 'auto',
                  borderRight: '2px solid',
                }}
                orientation='vertical'
                flexItem
              />
              <Tooltip
                title={`Average rating: ${addonData.averageRating}
                ${
                  rating ||
                  Object.keys(addonData.rating).includes(userData?.handle)
                    ? ' Your rating: ' +
                      (rating === 0
                        ? addonData.rating[userData?.handle]
                        : rating)
                    : ''
                }
              `}
              >
                <Typography variant='caption'>
                  <Rating
                    readOnly={!userData?.handle}
                    size='small'
                    value={
                      rating ||
                      Object.keys(addonData.rating).includes(userData?.handle)
                        ? rating === 0
                          ? addonData.rating[userData?.handle]
                          : rating
                        : addonData.averageRating
                    }
                    precision={0.5}
                    onChange={handleRating}
                    sx={{ color: 'darkred', padding: '8px' }}
                  />
                </Typography>
              </Tooltip>
              <Divider
                sx={{
                  height: '20px',
                  margin: 'auto',
                  borderRight: '2px solid',
                }}
                orientation='vertical'
                flexItem
              />
              <Typography variant='body2' p={1}>
                {addonData.price !== 'FREE'
                  ? `$${addonData.price}`
                  : addonData.price}
              </Typography>
            </Box>
          </Grid>
          <Grid item>
            <Typography variant='subtitle1'>{description}</Typography>
          </Grid>
          <Grid item mt={3}>
            {addonData.price === 'FREE' || purchasedByUser ? (
              <Button
                onClick={handleDownload}
                href={addonData.file}
                variant='contained'
                size='large'
              >
                Download
              </Button>
            ) : (
              <Box maxWidth={130}>
                <PayPalButtons
                  disabled={!userData?.handle}
                  style={{
                    layout: 'horizontal',
                    color: 'black',
                    height: 42,
                    label: 'buynow',
                  }}
                  createOrder={(_, actions) => {
                    return actions.order
                      .create({
                        purchase_units: [
                          {
                            amount: {
                              value: addonData.price,
                              breakdown: {
                                item_total: {
                                  currency_code: 'USD',
                                  value: addonData.price,
                                },
                              },
                            },
                            items: [
                              {
                                name: addonData.name,
                                quantity: '1',
                                unit_amount: {
                                  currency_code: 'USD',
                                  value: addonData.price,
                                },
                                category: 'DIGITAL_GOODS',
                              },
                            ],
                          },
                        ],
                      })
                      .then((orderId) => {
                        return orderId;
                      });
                  }}
                  onApprove={(data, actions) => {
                    return actions.order.capture().then(() => handlePurchase());
                  }}
                />
              </Box>
            )}
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
}
