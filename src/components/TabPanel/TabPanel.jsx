import Box from '@mui/material/Box';

const TabPanel = ({ children, value, index, ...rest }) => {
  return (
    <div
      role={'tabpanel'}
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...rest}
    >
      {value === index && (
        <Box p={3} pt={0}>
          {children}
        </Box>
      )}
    </div>
  );
};

export default TabPanel;
