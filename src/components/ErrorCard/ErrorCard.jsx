import Typography  from '@mui/material/Typography';
import { createTheme, ThemeProvider, styled } from '@mui/material/styles';
import ReportGmailerrorredIcon from '@mui/icons-material/ReportGmailerrorred';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';

const Item = styled(Paper)(({ theme }) => ({
  ...theme.typography.body2,
  color: theme.palette.text.secondary,
  minHeight: 200,
  width: 650,
  lineHeight: '60px',
}));

export default function ErrorCard({ header, message }) { 
  return (
    <Item elevation={2}>
      <Grid item fullWith sx={{backgroundColor:'red'}} minHeight='50px'>
        <Grid
        item
        display='flex'
        alignItems='center'
        >
          <ReportGmailerrorredIcon
          sx={{color:'white',
          fontSize:'50px',
          }}
          />
            
        </Grid>
      </Grid>
      <Typography 
      mt={4}
      color='red'
      align='center'
      fontSize='2.0rem'
      fontWeight='500'
      sx={{flex:'0 0 auto'}}
      >
        { header }
      </Typography>
      <Typography
      align='center'
      fontSize='1.8rem'
      >
        { message }
      </Typography>
    </Item>
  )
}