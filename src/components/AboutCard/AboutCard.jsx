import {React, useState} from 'react';
import { styled } from '@mui/material/styles';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import Collapse from '@mui/material/Collapse';
import Avatar from '@mui/material/Avatar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import { red } from '@mui/material/colors';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import Snackbar from '@mui/material/Snackbar';
import LinkedInIcon from '@mui/icons-material/LinkedIn';
import FacebookIcon from '@mui/icons-material/Facebook';
import GitHubIcon from '@mui/icons-material/GitHub';
import EmailIcon from '@mui/icons-material/Email';
import './AbourCard.css'

const ExpandMore = styled((props) => {
  const { expand, ...other } = props;
  return <IconButton {...other} />;
})(({ theme, expand }) => ({
  transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
  marginLeft: 'auto',
  transition: theme.transitions.create('transform', {
    duration: theme.transitions.duration.shortest,
  }),
}));


    export default function AboutCard ( { userInfo }) {
  
  const user = userInfo;
  const [expanded, setExpanded] = useState(false);
  const [open, setOpen] = useState(false);
  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  const handleClick = (value) => {
    setOpen(true);
    navigator.clipboard.writeText(value);
  };


  return (
    <Card
    sx={{ width: 300, border:1, borderColor:'#1976d2'}}
    >
      <CardHeader
        avatar={
          <Avatar 
          
          sx={{ bgcolor: red[400] }} aria-label="user name">
            {user.name[0].toUpperCase() + user.name.split(' ')[1][0].toUpperCase()}
          </Avatar>
        }
        title={<Typography align='center' variant='h6'>{user.name}</Typography>}
        subheader={<Typography  m='auto' align='center' variant='h8' sx={{display:'flex', justifyContent: 'center'}}>{user.title}</Typography>}
      />
      <CardMedia
        component="img"
        height="220"
        image={user.photo}
        alt={user.name}
      />
      <CardContent>
        <Typography variant="body2" color="text.secondary">
          Location: {user.currentCity}
        </Typography>
      </CardContent>
      <CardActions disableSpacing>
        <a href={user.github} target='_blank' rel='noreferrer'>
          <IconButton
          aria-label="github"
          >
            <GitHubIcon fontSize='large' sx={{color : '#1b1e23'}}/>
          </IconButton>
        </a>
        <a href={user.linkedIn} target='_blank' rel='noreferrer'>
          <IconButton aria-label="linkedIn">
            <LinkedInIcon  fontSize='large' sx={{color: '#0a66c2'}}/>
          </IconButton>
        </a>
          <IconButton
          aria-label="mail"
          onClick={() => handleClick(user.mail)}
          >
            <EmailIcon  fontSize='large' sx={{color: '#d54b3d'}}/>
          </IconButton>
    
        <a href={user.facebook} target='_blank' rel='noreferrer'>
          <IconButton aria-label="facebook">
            <FacebookIcon  fontSize='large' sx={{color: '#1877f2'}}/>
          </IconButton>
        </a>
        <Snackbar
        message="Copied to clipboard"
        anchorOrigin={{ vertical: "top", horizontal: "center" }}
        autoHideDuration={1500}
        onClose={() => setOpen(false)}
        open={open}
        />
        <ExpandMore
          expand={expanded}
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="show more"
        >
          <ExpandMoreIcon />
        </ExpandMore>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
          <Typography paragraph>About:</Typography>
          <Typography paragraph>
            {user.biography}
          </Typography>
        </CardContent>
      </Collapse>
    </Card>
  )
}
